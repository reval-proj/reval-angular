import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';


import { AppComponent } from './tags/AInput';
import { RootApp } from './Root';


@NgModule({
  declarations: [
    AppComponent,RootApp
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [RootApp]
})
export class AppModule { }
