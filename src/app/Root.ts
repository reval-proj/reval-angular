import { Component} from '@angular/core';
 
@Component({
    selector: 'root-app',
    templateUrl: './Root.html'
})
export class RootApp {
    errorStyle = {color: "green"};
}