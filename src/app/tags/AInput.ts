import { Component, Input, OnInit } from '@angular/core';
import {AbstractValidator} from 'reval-core';

@Component({
  selector: 'AInput',
  templateUrl: './AInput.html',
  styleUrls: ['./AInput.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  inputValidators: any;
  state: any;

  @Input() type = null;
  @Input() mode = null;
  @Input() validate = null;
  @Input() errorStyle = null;
  @Input() validStyle = null;
  @Input() adSet = null;
  @Input() length = null;
  @Input() value = null;

  ngOnInit() {
    this.inputValidators = (new AbstractValidator()).getValidators(this.type);

    let props = {
      type: this.type,
      mode: this.mode,
      validate: this.validate,
      errorStyle: this.errorStyle,
      validStyle: this.validStyle,
      adSet: this.adSet,
      length: this.length,
    }
    
    this.state = {...this.inputValidators.preValidator.getProperties(props)};
    this.state.style = {};
  }

  getClass() {
    return this.state.className || this.state.validClassName
  }

  onChange(event: any) {
    let newValue = event.target.value;
    let validObj = this.inputValidators.validator.validation(this.state, newValue);

    let changeData = {
        value: newValue,
        style: this.state.validStyle,
        className: this.state.validClassName
    }
    
    if (!validObj.isValid) {
        changeData = this.inputValidators.postValidator.doAction(changeData, this.state);
    }
    
    this.value = changeData.value;
    event.target.value = this.value;
    this.state = {...this.state,...changeData};
  }
}

